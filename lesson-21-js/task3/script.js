'use strict';

function pow (x, n) {
    var a = x;

    for (var i = 1; i < n; i++) {
        a *= x;
    }
    return a;
}

console.log (pow(3, 3));
console.log('--------------------------');
console.log (pow(2, 4));
console.log('--------------------------');
console.log (pow(5, 5));